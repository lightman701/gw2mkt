import requests
from tqdm.auto import tqdm

class GW2Api():
    def __init__(self):
        self.items_base = "https://api.guildwars2.com/v2/items"
        self.prices_base = "https://api.guildwars2.com/v2/commerce/prices"
        self.listings_base = "https://api.guildwars2.com/v2/commerce/listings"
        self.id_suffix = "?ids="
        # Not explicitly using the "page" feature of the API, instead only requesting data that keeps to one page
        # Helps avoid race conditions causing misses or duplicates in requests
        self.pageSize = 200

    def iterateOverItems(self, request_base, allIds, response):
        # Allow for a variable base request, the list of all IDs to iterate over (in batches of the page size),
        # and a variable response function downselecting and/or formatting the response to the paged request
        print("Performing request {} over {} items".format(request_base, len(allIds)), flush=True)
        nextId = 0
        failures = 0
        fullResponse = []
        with tqdm(total=len(allIds)) as pbar:
            while nextId < len(allIds):
                maxRequestId = min(nextId+self.pageSize - 1, len(allIds))
                #print("Requesting items {} to {} of {}".format(nextId, maxRequestId, len(allIds)), flush=True)
                page = requests.get(request_base + self.id_suffix + ','.join(str(id) for id in allIds[nextId:maxRequestId]))
                if page.ok:
                    fullResponse.extend(response(page.json()))
                    #update with actual number requested
                    pbar.update(maxRequestId - nextId + 1)
                    nextId += self.pageSize
                    failures = 0
                elif page.status_code == 404 and "all ids provided are invalid" in page.json()['text']:
                    # Look for explicit error message used to report that none of the requested IDs have any data
                    #update with actual number requested
                    pbar.update(maxRequestId - nextId + 1)
                    nextId += self.pageSize
                    failures = 0
                else:
                    print("Error with request: {}\n{}\nContent: {}".format(page.status_code, page.reason, page.json()))
                    failures += 1
                    if failures >= 2:
                        #page.raise_for_status()
                        print("Too many failures. Skipping.")
                        #update with actual number requested
                        pbar.update(maxRequestId - nextId + 1)
                        nextId += self.pageSize
                    else:
                        os.sleep(5)
                        print("Retrying", flush = True)

        return fullResponse

    def selectSellable(self, itemList):
        return [item for item in itemList if not 'NoSell' in item['flags']]

    def formatPrices(self, pricesList):
        return [(item['id'], item['buys']['unit_price'], item['buys']['quantity'], 
            item['sells']['unit_price'], item['sells']['quantity']) for item in pricesList]

    def getMarketItems(self):
        #Get all item IDs, exempting items that cannot be sold
        # WARNING! This function takes a while to run because it must do hundreds of API calls, understandably throttled
        allIds = requests.get(self.items_base)
        return self.iterateOverItems(self.items_base, allIds.json(), self.selectSellable)

    def getPrices(self, idList):
        # Get all buy and sell prices for each item in the list
        # WARNING! This function takes a while to run because it must do hundreds of API calls, understandably throttled
        return self.iterateOverItems(self.prices_base, idList, self.formatPrices)
