#!../Scripts/python
import datetime
from PricingDatabase import PricingDatabase
from GW2Api import GW2Api

ts = datetime.datetime.now()
api = GW2Api()
pd = PricingDatabase("localPricesDB.db", False)
pd.importPrices(ts, api.getPrices(pd.getIdList()))

