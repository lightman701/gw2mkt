import sqlite3
import os
import requests
from tqdm.auto import tqdm
from GW2Api import GW2Api

class PricingDatabase():
    def __init__(self, db_file, consistencyCheck = False):


        # Probably makes things not thread safe. No expectation of multithreading right now.
        self.conn = sqlite3.connect(db_file)
        self.curs = self.conn.cursor()
        createItemsTable = False
        createPricesTable = False
        sqlTableCheck = "SELECT name FROM sqlite_master WHERE type='table' AND name=?"
        print("Checking tables")
        if not os.path.isfile(db_file):
            createItemsTable = True
            createPricesTable = True
        else:
            self.curs.execute(sqlTableCheck, ('items',))
            itemresult = self.curs.fetchall()
            if itemresult == []:
                createItemsTable = True
            self.curs.execute(sqlTableCheck, ('prices',))
            pricesresult = self.curs.fetchall()
            if pricesresult == []:
                createPricesTable = True

        sqlItemInsert = "INSERT INTO items(id, name, type) VALUES(?, ?, ?);"
        if createItemsTable:
            # Create item table for basis of future comparison, selection, and details
            print("Creating items table")
            sqlCreateItemDescriptions = """CREATE TABLE items (
                id integer primary key,
                name text,
                type text
                );"""
            print("Getting items")
            self.curs.execute(sqlCreateItemDescriptions)
            api = GW2Api()
            items = api.getMarketItems()
            # Populate the item table
            for item in items:
                self.curs.execute(sqlItemInsert, [item['id'], item['name'], item['type']])
            self.conn.commit()

        if createPricesTable:
            print("Creating prices table")
            sqlCreatePrices = """CREATE TABLE prices (
                id integer,
                ts timestamp,
                buy_price integer,
                buy_quantity integer,
                sell_price integer,
                sell_quantity integer,
                PRIMARY KEY (id, ts)
                );"""
            self.curs.execute(sqlCreatePrices)
        elif consistencyCheck:
            #Get existing columns for comparison
            api = GW2Api()
            items = api.getMarketItems()
            self.curs.execute("SELECT * FROM items")
            storedItems = self.curs.fetchall()
            # Quick check on length to identify mismatch
            if len(items) != len(storedItems):
                for item in tqdm(items):
                    storedItem = self.getItemDetails(item['id'])
                    if len(storedItem) == 0:
                        print(f"Adding new item {item['id']}:{item['name']}")
                        self.curs.execute(sqlItemInsert, [item['id'], item['name'], item['type']])
                    elif item['id'] != storedItem[0][0] or item['name'] != storedItem[0][1]:
                        print("Inconsistency between items: API ID: {}\tAPI Name: {}\tDB ID: {}\tDB Name: {}".format(
                            item['id'], items['name'], storedItem[0][0], storedItem[0][1]))
                self.conn.commit()

    def getIdList(self):
        self.curs.execute("SELECT id FROM items")
        return [item[0] for item in self.curs.fetchall()]

    def importPrices(self, ts, prices):
        # Convert tuple (id, buy_price, buy_quantity, sell_price, sell_quantity) into sql to import
        sqlInsertGeneric = "INSERT INTO prices(id, ts, buy_price, buy_quantity, sell_price, sell_quantity) values (?, ?, ?, ?, ?, ?);"
        for item in prices:
            self.curs.execute(sqlInsertGeneric.format("buy"), (item[0], ts, item[1] if item[2] != 0 else None, item[2], item[3] if item[4] != 0 else None, item[4]))
        self.conn.commit()

    def getAllPrices(self, laterThan):
        self.curs.execute("SELECT * FROM prices WHERE ts > ?", (laterThan,))
        return self.curs.fetchall()
    
    def getPrices(self, itemId, laterThan):
        self.curs.execute("SELECT * FROM prices WHERE id = ? AND ts > ?", (itemId,laterThan))
        return self.curs.fetchall()

    def getPandasExtractionDetails(self):
        # Not importing pandas here. Attempting to keep this lightweight since extract does not require pandas
        # Return tuple (query, con, index_col, parse_dates) for use with https://pandas.pydata.org/docs/reference/api/pandas.read_sql_query.html#pandas.read_sql_query
        return ("SELECT * FROM prices WHERE ts > ?", self.conn, ['id', 'ts'], 'ts')

    def getPandasSaveDetails(self):
        # Not importing pandas here. Attempting to keep this lightweight since extract does not require pandas
        # Return tuple (tablename, con, index_col) for use with https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_sql.html#pandas.DataFrame.to_sql
        return ("prices", self.conn, ['id', 'ts'])

    def getPandasAnalysisSaveDetails(self):
        return ("analysis", self.conn, 'id')

    def getPandasAnalysisExtractionDetails(self):
        return ("SELECT * FROM analysis", self.conn, 'id')

    def getPandasItemExtractionDetails(self, id, ts):
        return ("SELECT * FROM prices where id = ? AND ts > ?", self.conn, (id, ts), 'ts')
    
    def searchForItem(self, filter):
        self.curs.execute("SELECT * FROM items WHERE name LIKE ?", (filter,))
        return self.curs.fetchall()

    def getItemDetails(self, id):
        self.curs.execute("SELECT * FROM items where id = ?", (id,))
        return self.curs.fetchall()

    def truncate(self, ts):
        self.curs.execute("DELETE FROM prices WHERE ts < ?", (ts,))
        self.conn.commit()
