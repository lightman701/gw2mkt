Initial Step
Source python environment using the method required for your terminal. This guide assumes a Git Bash terminal in Windows.
```source Scripts/activate```

To get data from the API and save it in the database:
```./extract.py```

Development and data view is available from Jupyter
```jupyter lab```
