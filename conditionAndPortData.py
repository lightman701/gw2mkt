# Transition data from the raw storage to a compressed set
# Goal is to save all relevant data in as few rows as possible
# And have data conditioned for easier processing later
from PricingDatabase import PricingDatabase
import datetime
import numpy as np
import pandas as pd
from concurrent.futures import ThreadPoolExecutor, as_completed
from tqdm.auto import tqdm

extractionPeriod = 0.5 # hrs
# https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#timeseries-offset-aliases
freqStr = '30T' # half an hour

# Open the two databases, one with the raw collected data, the other to store conditioned data
try:
    nextfile
    print(f"Using {nextfile} to load prices")
except:
    nextfile = 'localPricesDB.db'
origindb = PricingDatabase(nextfile, False)
cleandb = PricingDatabase('conditionedPricesDB.db', False)

# Get the raw data and the list of items to parallelize over
details = origindb.getPandasExtractionDetails()
alldata = pd.read_sql_query(sql = details[0], con = details[1], params = (datetime.datetime.min,), parse_dates = details[3])
allIds = alldata['id'].unique()

# The function to run to condition the data for each item.
# Creates a new dataframe with the minimum possible rows with a standardized index
# Several assumptions about re-expanding the data are made and described below
def conditionData(itemId):
    itemdata = alldata[alldata['id'] == itemId]
    # Make index indicate it's proper interval/initialization time
    # This will allow identification of period of data used for statsmodels
    itemdata.set_index(keys='ts', inplace = True)
    itemdata.index = itemdata.index.floor(freq=freqStr)
    # Eliminate redundant timestamps
    itemdata = itemdata[~itemdata.index.duplicated(keep='first')]
    # There might be data missing due to sampling failure that might make gaps in the indicies so
    # they aren't periodic. That is okay here. Those gaps are expected to be filled when processing.
    # This script is only intended to reduce the data to a savable state, not make it periodic by
    # coming up with implied data

    # Because of the above mentioned possibility of missed data, a fill (ffill, bfill) or interpolation
    # is expected to be run on the data. This subsequently provides the option to eliminate data that
    # will be repopulated by these functions, to further reduce storage used. Any data that isn't
    # transacted frequently will have redundant data that we can eliminate all but the edges and still
    # recover all explicit data. DO NOT use drop_duplicates as data can change, then change back. We
    # don't want to drop the change back
    samedata = pd.DataFrame(data=None, columns=itemdata.columns)
    for index, row in itemdata.iterrows():
        # if we don't have any data to compare against, save it
        if len(samedata.index) == 0:
            samedata = row.to_frame().T
            continue
        # otherwise compare the current row with the last row
        identical = True # same until proven otherwise by any column
        for col, val in row.items():
            # Compare each column. Stop if something differs
            if col != 'ts' and col != 'id' and val != samedata.iloc[-1][col]:
                identical = False
                break
            # if we get to the end of the loop then they're identical
        if identical:
            # save this row so we can get the list of items between that we should drop
            samedata = pd.concat([samedata, row.to_frame().T], join='inner')
        # If they're not identical, or we're at the end of the data, cut out the excess
        if not identical or index == itemdata.index[-1]:
            #Make sure we have more than the two endpoints, otherwise we're not dropping anything
            if len(samedata.index) > 2:
                # Remove everything except the first and last in this identical set from the full data set
                samedata.drop(index=samedata.index[0], inplace=True)
                samedata.drop(index=samedata.index[-1], inplace=True)
                itemdata = itemdata.drop(index=samedata.index)
            # Now clear out this data set and start collecting for the next one
            samedata = row.to_frame().T       

    return itemdata

#allIds = allIds[0:200]
print(f"Conditioning data for {len(allIds)} items", flush=True)
conditionedData = pd.DataFrame(data=None, columns=alldata.columns)
with ThreadPoolExecutor() as executor:
#                                        itemdata = alldata[alldata['id'] == itemId]): itemId
    futureToId = {executor.submit(conditionData,
                                  itemId): itemId
                  for itemId in tqdm(allIds)}
    print("All jobs prepared")
    for future in tqdm(as_completed(futureToId), total=len(allIds)):
        try:
            # Merge resulting conditioned dataframes
            conditionedData = pd.concat([conditionedData, future.result()], join='inner')
        except:
            print(f"Error with item {futureToId[future]}: {future.exception()}")

# At this stage, all unnecessary data and rows are eliminated. Lastly, cleanup what we'll keep.
# Adjust data type to something smaller than 64-bit floats (even if things are switched back to object)
for col, val in conditionedData.items():
    if col != 'ts':
        conditionedData[col] = pd.to_numeric(conditionedData[col], downcast='unsigned', errors='ignore')   # Adjust index to support combination after this function completes
conditionedData.set_index(keys='id', inplace = True, append=True)
# Prices were set to 0 if no buys or sells existed. Not helpful for statistics. NaN makes that be skipped.
conditionedData['sell_price'] = conditionedData.apply(lambda conditionedData: np.nan if conditionedData['sell_quantity']==0 else conditionedData['sell_price'], axis=1)
conditionedData['buy_price'] = conditionedData.apply(lambda conditionedData: np.nan if conditionedData['buy_quantity']==0 else conditionedData['buy_price'], axis=1)

# Drop data that already exists in the database to avoid errors with collision
loadDetails = cleandb.getPandasExtractionDetails()
olddata = pd.read_sql_query(sql = loadDetails[0], con = loadDetails[1], params = (datetime.datetime.min,), parse_dates = loadDetails[3])
olddata.set_index(keys=['ts', 'id'], inplace=True)
conditionedData.drop(olddata.index, errors='ignore', inplace=True)

saveDetails = cleandb.getPandasSaveDetails()
conditionedData.info()
conditionedData.to_sql(name=saveDetails[0], con=saveDetails[1], index=True, 
                       index_label=['ts','id'], if_exists='append')
#                       if_exists='replace')
