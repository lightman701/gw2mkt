from PricingDatabase import PricingDatabase
from GW2Api import GW2Api
import pandas as pd
import numpy as np
import datetime
from concurrent.futures import ThreadPoolExecutor, as_completed
from tqdm.auto import tqdm

from ItemStatistics import ItemStatistics

pdb = PricingDatabase('conditionedPricesDB.db', False)

# Let's see if we can make some profit off of commodities!
# Load the data into Pandas!
details = pdb.getPandasExtractionDetails()
alldata = pd.read_sql_query(sql = details[0], con = details[1], params = (datetime.datetime.min,), parse_dates = details[3])

# restrict to the items that actually able to be bought (get) at a lower price than we could sell, at any point in time
# Market does force that buy_price will always be lower than sell_price at any given sample. But we want to do the transactions at different points in time
# Wiping out things that never met the criteria to buy low and sell higher cuts down on processing
print("Filtering irrelevant items", flush=True)
best_get = alldata[['id', 'buy_price']].groupby('id').min().rename(
    columns={'buy_price':'best_val'})
best_drop = alldata[['id', 'sell_price']].groupby('id').max().rename(
    columns={'sell_price':'best_val'}) * 0.85
itemvals = best_drop - best_get
itemvals.dropna(inplace = True)
# If you can't make more than 10 copper selling a single unit, it's probably not worth it
validitems = itemvals[itemvals['best_val'] > 10]
# TEMP TROUBLESHOOTING
#validitems = validitems[validitems.index == 19700]
# Select only the ids that meet our above criteria. That's our new dataframe to find ways to profit
filtereddata = pd.merge(alldata, validitems, how = 'inner', on = ['id', 'id'])

# Multithread the processing of each item since it won't collide (and it could take a while)
print(f"Starting individual item processing for {len(validitems.index)} items", flush=True)
try:
    maxInvest
except:
    maxInvest = float('inf')
items = {}
with ThreadPoolExecutor() as executor:
    future_to_result = {executor.submit(ItemStatistics, 
                                        itemId = itemId,
                                        itemDetails = pdb.getItemDetails(itemId),
                                        itemdata = filtereddata[filtereddata['id'] == itemId],
                                        maxInvest = maxInvest,
                                       ): itemId 
                        for itemId in tqdm(validitems.index)}
    for future in tqdm(as_completed(future_to_result), total = len(validitems.index)):
        try:
            # Expecting ItemStatistics to have several summary values for comparison computed
            # Decide what to do with them after calculation. For now, just store
            items[future_to_result[future]] = future.result()
        except:
            print(f"Error with item {future_to_result[future]}: {future.exception()}")

itemResults = {key:(item.name,
                    item.category,
                    item.volumePerDay,
                    item.idealPercentGain, 
                    item.profit,
                    item.forecastProfit,
                    item.forecastPercentGain,
                    item.forecastVolume,
                    item.forecastBuyPrice,
                    ) for (key, item) in items.items()}
itemResults = pd.DataFrame(itemResults.values(), columns=[
                            'Name',
                            'Category',
                            'Volume Per Day',
                            'Ideal Percent Gain',
                            'Profit',
                            'Forecasted Profit',
                            'Forecasted Percent Gain',
                            'Forecast Volume',
                            'Forecast Buy Price',
                            ], index=itemResults.keys())

#itemResults = itemResults[itemResults['Forecasted Profit'] > 0 or itemResults['Profit'] > 0]
#itemResults = itemResults[itemResults['Yesterdays Gold Exchange'] > 0]

saveDetails = pdb.getPandasAnalysisSaveDetails()
itemResults.to_sql(name=saveDetails[0], con=saveDetails[1], index=True, 
                   index_label=saveDetails[2], if_exists='replace')

#doesn't print as nicely in Jupyter as just having it return
#print(itemResults.sort_values(by = 'Profit', ascending = False).head(50))