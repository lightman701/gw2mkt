import pandas as pd
import numpy as np
import warnings
from statsmodels.tsa.api import ExponentialSmoothing

extractionPeriod = 0.5 # hrs
# https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#timeseries-offset-aliases
freqStr = '30T' # half an hour
marketFeePercent = 0.15
forecastLength = int(24/extractionPeriod)

# Enter statsmodels
# https://www.statsmodels.org/stable/generated/statsmodels.tsa.holtwinters.ExponentialSmoothing.html
# https://www.statsmodels.org/stable/examples/notebooks/generated/exponential_smoothing.html#Holt's-Winters-Seasonal
# This will help identify seasonal trends, but has a lot of caveats in order to ingest the data...

def rms(fit, actual):
    return np.sqrt(((fit - actual) ** 2).mean())

class ItemStatistics():
    def __init__(self, itemId, itemDetails, itemdata, maxInvest=float('inf')):
        self.itemId = itemId
        if len(itemDetails) > 0:
            self.name = itemDetails[0][1]
            self.category = itemDetails[0][2]
        else:
            self.name = "Unknown"
            self.category = "Unknown"
        self.itemdata = self.regulateIndex(itemdata)
        self.calculateVolumeAndExchange()
        self.calculateProfitFromHistorical(maxInvest)
        self.calculateProfitFromForecast(maxInvest)
        
        # Notes on goals:
        # - Acquire - put up cheapest possible buy order or buy from really cheap sell order
        # - - Really cheap sell order depends on outlier algorithm
        # - - Buy order - best price is point of highest volume lower than mean, higher than min
        # - Offload - put up sell order or sell to really expensive buy order
        # - - Really expensive buy order depends on outlier algorithm
        # - - Sell order - best price is point of highest volume higher than mean, lower than max
        
        # Homebrew convolution is taking too long. Who'd have thought?
        #self.calculatePercentageGain()

        #switch back to dateindex to get gold exchange for last 24 hours
        #self.dailyExchange = self.itemdata['exchange'].last('D').sum()
        #Get gold exchange per day to identify/eliminate rarely transacted items
        self.dailyExchange = self.itemdata['exchange'].resample('D').sum()
        #Get gold exchange for last complete day recorded
        self.yesterdaysExchange = self.dailyExchange.iloc[-2]
        #Get average volume traded per day
        self.volumePerDay = self.itemdata['volume'].resample('D').sum().mean()
        self.exchangePerDay = self.itemdata['exchange'].resample('D').sum().mean()

        #should this be explored...
        #self.minMaxData = self.itemdata[['buy_price','sell_price']].resample('2H').max()
    

    def calculateProfit(self, buyPrice, sellPrice, volume):
        return ((1-marketFeePercent)*sellPrice - buyPrice) * volume
    
    def regulateIndex(self, data):
        # statsmodels wants a Pandas DateIndex.
        data.set_index(keys = 'ts', inplace = True)
        # statsmodels uses the inferred_freq, which doesn't work unless the timestamps are uniform.
        # force the timestamps to associate with the time they were triggered
        # Also, it will use that as a comparison if you do feed it a freq independently, and the comparison failing makes it upset
        # https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#timeseries-offset-aliases
        data.index = data.index.floor(freq='30T')
        # Get rid of duplicates since there were apparent mistakes in collection
        data = data[~data.index.duplicated(keep='first')]
        # in order to drive home the uniform timestamps, any missed data must be filled in. Create a regular spacing across the data
        # using this as the index will inject empty rows
        dailyindex = pd.period_range(start=data.index.min(), 
                                     end=data.index.max(), 
                                     freq='30T', name='cont_ts')
        data = data.reindex(dailyindex.to_timestamp())
        # having that inferred_freq still didn't seem to be enough, so make it a Pandas PeriodIndex to really drive the point home
        data.index = data.index.to_period()
        # Despite specifying missing="drop", the NaNs still count against being able to do multiplicative treading and seasons
        # fillna works well but can exasterbate oddities. Data may not populate at the beginning of the table if there was a buy and no sell, or vice versa,
        # so a forward fill and a backward fill are both required
        # Alternatively interpolate can fill those in one shot, but it creates data that wasn't real. Still might be the best estimate though...
        #data.fillna(method='ffill', inplace=True)
        #data.fillna(method='bfill', inplace=True)
        data.interpolate(limit_direction='both', inplace=True)
        return data

    def calculateVolumeAndExchange(self):
        prevdata = self.itemdata.shift()
        prevdata.rename(columns={'buy_price':'prev_buy_price',
                 'sell_price':'prev_sell_price',
                 'buy_quantity':'prev_buy_quantity',
                 'sell_quantity':'prev_sell_quantity',
                }, inplace=True)
        prevdata = prevdata.merge(self.itemdata, on='cont_ts')
        buyvolume = (prevdata['prev_buy_quantity'] - prevdata['buy_quantity'])
        buyvolume[buyvolume < 0] = 0
        self.itemdata['buyvolume'] = buyvolume
        sellvolume = (prevdata['prev_sell_quantity'] - prevdata['sell_quantity'])
        sellvolume[sellvolume < 0] = 0
        self.itemdata['sellvolume'] = sellvolume
        self.itemdata['volume'] = buyvolume+sellvolume
        buy_transaction_price = prevdata[['buy_price','prev_buy_price']].min(axis=1)
        buy_transaction_price[buyvolume == 0] = np.NAN
        self.itemdata['buy_transaction_price'] = buy_transaction_price
        sellTransactionPrice = prevdata[['sell_price','prev_sell_price']].min(axis=1)
        sellTransactionPrice[sellvolume == 0] = np.NAN
        self.itemdata['sell_transaction_price'] = sellTransactionPrice
        self.itemdata['exchange'] = buyvolume * buy_transaction_price \
            + sellvolume * sellTransactionPrice

    def calculateProfitFromHistorical(self, maxInvest):       
        # Get Buy Data below mean and Sell Data above mean to reduce data to process to most likely candidates
        self.buyRangeData = self.itemdata[self.itemdata['buy_transaction_price'] <= self.itemdata['buy_transaction_price'].mean()]
        self.buyRangeData = self.buyRangeData[self.buyRangeData['volume'] >
            self.buyRangeData['volume'].mean()]
        self.sellRangeData = self.itemdata[self.itemdata['sell_transaction_price'] <= self.itemdata['sell_transaction_price'].mean()]
        self.sellRangeData = self.sellRangeData[self.sellRangeData['volume'] >
            self.sellRangeData['volume'].mean()]

        self.profit = float('-inf')
        self.idealBuyPrice = 0
        self.idealSellPrice = 0
        # don't actually buy this much, this is what was transacted
        self.volumeAtIdeal = 0
        self.idealPercentGain = 0
        # iterate over all the rows. original algorithm just picked out prices at max volumes,
        # but it was found that there were sufficient volumes below the max that were at lower
        # prices that would make the item more viable for investment
        for bInd, bRow in self.buyRangeData.iterrows():
            for sInd, sRow in self.sellRangeData.iterrows():
                worldVolume = np.min([bRow['volume'], sRow['volume']])
                myVolume = np.floor(np.min([worldVolume * bRow['buy_transaction_price'],
                                          maxInvest] / bRow['buy_transaction_price']))
                thisProfit = self.calculateProfit(bRow['buy_transaction_price'],
                                                  sRow['sell_transaction_price'],
                                                  myVolume)
                if thisProfit > self.profit:
                    self.profit = thisProfit
                    self.idealBuyPrice = bRow['buy_transaction_price']
                    self.idealSellPrice = sRow['sell_transaction_price']
                    # don't actually buy this much, this is what was transacted
                    self.volumeAtIdeal = worldVolume
                    self.idealPercentGain = ((1-marketFeePercent)*self.idealSellPrice - self.idealBuyPrice) / self.idealBuyPrice * 100
    
    def extractSeasonalFit(self, data):
        # I knew using real data to test predictions would be better, but I wasn't expecting it
        # to be vital to finding a valid model. Didn't expect perfect fit with flat forecast...
        calcdata = data.iloc[:-forecastLength]
        testdata = data.iloc[-forecastLength:]
        # Go through multiple Expontential Smoothing arguments to determine which one has best fit
        # Bitwise alternate input parameters and use RMS to pick the best fit
        # Deprecated bit values based on minimal repetition through items and in an interest to 
        # speed up processing, as these calculations were taking unreasonably long
        # 1 = daily vs weekly
        # 2 = seasonal "add" vs "mul"
        # 4 = boxcox false or true - seemed to perform better as false
        # 8 = trend "add" vs "mul" - skipped for apparent insignificant difference in alternatives
        # 16 = damped trend false or true - skipped for apparent insignificant difference in alternatives
        fits = {}
        fitrms = {}
        for i in range(0,1):
            try:
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    fit = ExponentialSmoothing(
                        calcdata,
                        seasonal_periods = 24/extractionPeriod * (7 if (i & 1) else 1),
                        seasonal = "mul" if (i & 2) else "add",
                        use_boxcox = bool(i & 4),
                        trend = "mul" if (i & 8) else "add",
                        damped_trend = bool(i & 16),
                        missing="drop").fit()
                    fits[i] = fit
                    fitrms[i] = rms(fit.forecast(forecastLength), testdata)
            except Exception as e: 
                # If it fails, skip it. Boxcox won't like the zeros that sometimes appear in volume or
                # issues with data being flat
                # print(f"Item {self.itemId} Fit {i} failed: {e}")
                continue
        # Return the fit that most closely approximates the data, determined my Root Mean Square error
        self.fits = fits
        return fits[min(fitrms, key=fitrms.get)] if len(fits) > 0 else None

    def calculateProfitFromForecast(self, maxInvest):
            # This is spewing a lot of errors... wait on this one
        buyfit = self.extractSeasonalFit(self.itemdata['buy_price'])
        sellfit = self.extractSeasonalFit(self.itemdata['sell_price'])
        self.buyfit = buyfit
        self.sellfit = sellfit
        if not buyfit or not sellfit:
            self.forecastBuyPrice = 0
            self.forecastSellPrice = 0
            self.forecastPercentGain = 0
            self.forecastVolume = 0
            self.forecastProfit = 0
            return
        self.forecastBuyPrice = buyfit.forecast(forecastLength).min()
        self.forecastSellPrice = sellfit.forecast(forecastLength).max()
        priceDelta =  self.forecastSellPrice - self.forecastBuyPrice
        self.forecastPercentGain = ((1-marketFeePercent)*self.forecastSellPrice - self.forecastBuyPrice) / self.forecastBuyPrice * 100
        # https://stats.stackexchange.com/questions/454819/how-to-use-holt-winters-seasonal-multiplicative-method-when-the-data-has-zero-v
        # Despite adjustments recommended above, Holt's Winter's doesn't seem to like my volume data
        # Until that can be sorted out, use the historic volume to estimate potential profit

        # Estimate expected max volume at buy price based on mean of volumes of buys and sells 
        # at that price
        usefulnessCutoff = np.max([1, self.forecastBuyPrice * 0.01])
        # Method 1: find previous volume at this price
        # May not give valid results if this buy price has been skipped and there is volume at lower prices
        '''
        relbuyvals = (item.itemdata['buy_price']-self.forecastBuyPrice).abs()
        relbuyvals = relbuyvals[relbuyvals < usefulnessCutoff]
        relsellvals = (item.itemdata['sell_price']-self.forecastBuyPrice).abs()
        relsellvals = relsellvals[relsellvals < usefulnessCutoff]
        volumes = pd.concat(
            [item.itemdata[item.itemdata.index.isin(relbuyvals.index)]['buyvolume'],
         item.itemdata[item.itemdata.index.isin(relsellvals.index)]['sellvolume']]
        )
        '''
        # if no historic data at his price, take max volume lower
        # Method 2: Find max volume below intended price
        buyRangeData = self.itemdata[self.itemdata['buy_price'] <= \
            (self.forecastBuyPrice+usefulnessCutoff)]
        sellInBuyRangeData = self.itemdata[self.itemdata['sell_price'] <= \
            (self.forecastBuyPrice+usefulnessCutoff)]
        buyInSellRangeData = self.itemdata[self.itemdata['buy_price'] >= \
            (self.forecastSellPrice-usefulnessCutoff)]
        sellRangeData = self.itemdata[self.itemdata['sell_price'] >= \
            (self.forecastSellPrice-usefulnessCutoff)]
        # Going to use mean of all buy/sell data in range, not sum of max to ease away anomolies
        buyvolumes = pd.concat([buyRangeData['volume'], sellInBuyRangeData['volume']])
        sellvolumes = pd.concat([buyInSellRangeData['volume'], sellRangeData['volume']])
        volume = np.min([buyvolumes.mean(), sellvolumes.mean()])
        self.forecastVolume = np.floor(np.min([volume * self.forecastBuyPrice, 
                                                 maxInvest]) / self.forecastBuyPrice)
        self.forecastProfit = self.calculateProfit(self.forecastBuyPrice,
                                                   self.forecastSellPrice,
                                                   self.forecastVolume)
        

    # Analysis to Code
    # Calculate the gold exchange over time
    # Calculate the area of buy price below sell price across times - how to handle quatities
    # - maybe add a volume transacted column calculated during (or for) gold exchange, this
    #   won't give the number that could have been sold at a price, but it's something
    # Find bargain buys by comparing last price to current price, percent or # stddev below mean, or maybe below fitted/expected minimum
    
    
    
    # def calculateGoldExchange(row1, row2):
        
        
    # A modified convolution algorithm to obtain how much sell (get) was below buy (dump)
    def calculatePercentageGain(self):
        self.percentageGainPerUnit = 0
        self.percentageGain = 0
        for indexA, rowA in self.itemdata.iterrows():
            for indexB, rowB in self.itemdata.iterrows():
                percentageGainPerUnit = np.max([rowA['buy_price'] - rowB['sell_price'] / rowA['buy_price'], 0])
                quantity = rowA['volume'] + rowB['volume'] #probably wrong, but it should be a start
                self.percentageGainPerUnit += percentageGainPerUnit
                self.percentageGain += percentageGainPerUnit * quantity
